const express = require('express');
const axios = require('axios');
const path = require('path');

const app = express();

app.get('/fetch-users', async (req, res) => {
  try {
    const { data: users } = await axios.get('https://jsonplaceholder.typicode.com/users');
    res.send(users);
  } catch (err) {
    console.error(err);
  }
});

if (process.env.NODE_ENV === 'production') {
  console.log('In production');

  app.use(express.static('client/build'));

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log(`Server started on port: ${PORT}`));
