import axios from "axios";
import { FETCH_USERS } from "./types";

export const fetchUsers = () => async dispatch => {
   try {
      const res = await axios.get("/fetch-users");
      dispatch({ type: FETCH_USERS, payload: res.data });
   } catch (err) {
      console.log(err);
   }
};
