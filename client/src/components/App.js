import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../actions";

import "../App.css";

import Tile from "./Tile";

class App extends Component {
   constructor(props) {
      super(props);
      this.myRef = React.createRef();
   }

   state = {
      query: "",
      results: []
   };

   componentDidMount() {
      this.props.fetchUsers();
      this.moveFocus();
   }

   componentDidUpdate(prevProps) {
      if (prevProps.users !== this.props.users) {
         this.setState({ results: [...this.props.users] });
      }
   }

   search = query => {
      // const { query } = this.state;
      const { users } = this.props;
      if (query === "") {
         return this.setState({ query, results: [...users] });
      }
      const lcQuery = query.toLowerCase();
      const results = users.filter(user => {
         const { id, phone, address, company, ...rest } = user;
         const { zipcode, geo, ...addRest } = address;
         const { name: compName, catchPhrase } = company;
         const searchUser = { ...rest, ...addRest, compName, catchPhrase };
         let found = false;
         for (let key in searchUser) {
            found = found || searchUser[key].toLowerCase().includes(lcQuery);
            console.log(searchUser[key]);
         }
         return found;
      });
      this.setState({ query, results });
   };

   moveFocus = () => {
      const node = this.myRef.current;
      node.addEventListener("keydown", function(e) {
         const active = document.activeElement;
         if (e.keyCode === 40 && active.nextSibling) {
            active.nextSibling.focus();
         }
         if (e.keyCode === 38 && active.previousSibling) {
            active.previousSibling.focus();
         }
      });
   };

   render() {
      const { query, results } = this.state;
      return (
         <div className="App" ref={this.myRef}>
            <input
               autoFocus
               tabIndex="0"
               onChange={e => this.search(e.target.value)}
               id="search-input"
               placeholder="Type your query here"
               value={query}
            />

            {results.map((result, i) => {
               return <Tile key={i} tabIndex={i + 1} result={result} />;
            })}
         </div>
      );
   }
}

const mapStateToProps = ({ users }) => {
   return { users };
};

export default connect(
   mapStateToProps,
   actions
)(App);
