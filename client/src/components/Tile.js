import React from "react";

const displaySubTexts = (rawTexts, type = null) => {
   // Assuming that rawTexts is either an array or object
   let jsx,
      texts = {},
      counter = 0;
   if (Array.isArray(rawTexts)) {
      for (let item of rawTexts) {
         texts[counter] = item;
         counter++;
      }
   } else {
      texts = { ...rawTexts };
   }
   switch (type) {
      case "ul":
         jsx = (
            <ul>
               {Object.keys(texts).map(key => (
                  <li key={key}>{texts[key]}</li>
               ))}
            </ul>
         );
         break;
      case "p":
         jsx = Object.keys(texts).map(key => <p key={key}>{texts[key]}</p>);
         break;
      default:
         jsx = Object.keys(texts).map(key => (
            <div className="detail" key={key}>
               {texts[key]}
            </div>
         ));
   }
   return jsx;
};

const displayIconText = (src, alt, text) => {
   return (
      <div className="li-group">
         <img src={src} alt={alt} />
         {text}
      </div>
   );
};

const Tile = ({
   tabIndex,
   result: {
      name,
      username,
      email,
      address: { geo, ...lines },
      phone,
      website,
      company
   }
}) => {
   const API_KEY = "AIzaSyAgz_CErBr1auLCKex1UHk6G1aGqC71KiE";
   const basicDetails = [
      ` Username: ${username}`,
      displayIconText(
         "https://img.icons8.com/color/48/000000/secured-letter.png",
         "email icon",
         <a href={`mailto:${email}`}>{email}</a>
      ),
      displayIconText(
         "https://img.icons8.com/color/48/000000/phone.png",
         "phone icon",
         <a href={`tel:${phone}`}>{phone}</a>
      ),
      displayIconText(
         "https://img.icons8.com/color/48/000000/geography.png",
         "web icon",
         <a href={website} target="_blank" rel="noopener noreferrer">
            {website}
         </a>
      )
   ];
   return (
      <div className="tile" tabIndex={tabIndex}>
         <h1>{name}</h1>
         <div className="section-container">
            <div className="section center">
               {displaySubTexts(basicDetails)}
            </div>
            <div className="section">
               <h2>Address:</h2>
               {displaySubTexts(lines, "p")}
            </div>
            <div className="section">
               <h2>Location:</h2>
               <iframe
                  className="map"
                  title={username + "-map"}
                  width="200"
                  height="200"
                  src={`https://www.google.com/maps/embed/v1/place?key=${API_KEY}&q=${
                     geo.lat
                  },${geo.lng}&zoom=5`}
               />
            </div>
            <div className="section">
               <h2>Company:</h2>
               <div className="detail">{company.name}</div>
               <div className="detail italic">{company.catchPhrase}</div>
               <div className="detail italic">{company.bs}</div>
            </div>
         </div>
      </div>
   );
};

export default Tile;
